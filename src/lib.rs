///
///    libpassory - Shared library for common functionality within passory
///    Copyright (C) 2021  Marcus Pedersén marcus@marcux.org
///
///    This program is free software: you can redistribute it and/or modify
///    it under the terms of the GNU General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    This program is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU General Public License for more details.
///
///    You should have received a copy of the GNU General Public License
///    along with this program.  If not, see <http://www.gnu.org/licenses/>.
///



use serde::{Serialize, Deserialize};
use serde_json::Result;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{UnixListener, TcpListener};
use anyhow::{Context, Result};

/// All available commands that
/// can be used
pub enum Command {
    Add,
    Modify,
    Delete,
    Passwd,
    Search,
    StatusCheck,
    Response,
}

/// Trait to be used with NetCommand
/// all structs that should be used
/// must implement this trait
pub trait CommandData { }

#[derive(Serialize, Deserialize)]
pub struct User {
    pub uid = Option<u32>,
    pub name = Option<String>,
    pub description = Option<String>,
    pub primary_group = Option<String>,
    pub secondary_groups = Option<Vec<String>>,
    pub shell = Option<String>,
    pub home = Option<String>,
    pub mail = Option<String>,
    pub organisation = Option<String>,
    pub locked = Option<bool>,
    pub expire = Option<String>,
}
impl User {
    pub fn from_str(ser_user: &str) -> Result(User, String) {
        if let u: User = serde_json::from_str(ser_user) {
            if u.uid.is_some() && u.name.is_some() {
                Ok(u)
            }
	    else {
                Err(String::new("Error: User struct deserialized from json does not contain uid or name."))
            }
        }
	else {
            Err(String::new("Error parse serialized json to User struct"))
        }
    }
}
impl CommandData for User { }

#[derive(Serialize, Deserialize)]
pub struct Group {
    pub gid = Option<u32>,
    pub name = Option<String>,
    pub description = Option<String>,
}
impl Group {
    pub fn from_str(ser_group: &str) -> Result(Group, String) {
        if let g: Group = serde_json::from_str(ser_group) {
            if g.gid.is_some() || g.name.is_some() {
                Ok(g)
            }
	    else {
                Err(String::new("Error: Group struct deserialized from json does not contain gid or name."))
            }
        }
	else {
            Err(String::new("Error parse serialized json to Group struct"))
        }
    }
}
impl CommandData for Group { }


/// Struct to be sent over network
#[derive(Serialize, Deserialize)]
pub struct NetCommand<T: CommandData> {
    pub key: String,
    pub cmd: Command,
    pub data: Option<T>,
}
impl NetCommand {
    /// Reads from socket, converts to a
    /// utf8 string slice, deserializes
    /// to NetCommand that is returned.
    pub async fn read(socket: &AsyncReadExt ) -> Result<NetCommand> {
        let mut buf = Vec::new();
	let Ok(n) = socket.read(&buf).await.with_context("Can not read from socket")?

        if n <= 0 {
	    return Err("No bytes read from socket");
	}

	let utf8_str = std::str::from_utf8(&buf).with_context("Can not convert read vec to utf8 str")?
	let nc: NetCommand = serde_json::from_str(utf8_str).with_context("Can not deserialize json str to NetCommand struct)?
	Ok(nc) 
    }

    /// Serialize and writes this instance to
    /// specified socket.
    /// Will continue to call
    /// write until all data is
    /// written.
    pub async fn write(&self, socket: &AsyncWriteExt) -> Result<()>{
        let ser_json = serde_json::to_string(self).with_context("Can not serialize a NetCommand instance")?;
        socket.write_all(ser_json).await.with_context("Can not write an instance of NetCommand to socket")?;
	Ok(())
    }
}



#[derive(Serialize, Deserialize)]
pub struct Response {
    pub message: Result<String, String>,
}
impl CommandData for Response {}


#[derive(Serialize, Deserialize)]
pub struct SearchResponse {
    pub message: Result<String, String>,
    pub result: Vec<CommandData>,
}
impl CommandData for SearchResponse { }


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
